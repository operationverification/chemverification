<?php

$filePath = "wurcs_duplicate_biologic_cid.tsv";

$file = fopen($filePath, "r");
 
$url1 = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/CID/";
$url2 = "/record/SDF/?record_type=2d&response_type=display";


$result_sum = array();

if($file){

	$LineCount = 1;
  while ($line = fgets($file)) {
    echo "# LINE:".$LineCount.PHP_EOL;
    $LineCount++;

    //if($LineCount > 10) break;


    $result = array();

    $data = explode("\t", $line);

    if (count($data) == 3){
    	$oldWurcs = $data[0];
    	$t_cids = $data[1];
    	$pubchemUrl = $data[2];
		
			$cids = explode(" ", $t_cids);
			$ws = array();
			$aglycon = 0;
			$sdf = "";

			echo "# WURCS Duplicate Biologic CID:".count($cids)."\t".$pubchemUrl;
			foreach ($cids as $cid) {
	 			$url = $url1.$cid.$url2;

	 			array_push($result, $t_cids);

	 			$sdf = file_get_contents($url);

	 			$cmd = "curl -s \"".$url."\" | java -jar MolWURCS.jar --in sdf --out wurcs";

				exec($cmd, $opt, $return_ver);
				if (count($opt)>0) {
					echo $opt[0].PHP_EOL;

				  $wurcs = explode("\t", $opt[0]);
				  if (count($wurcs)>1) {
				    array_push($ws, $wurcs[1]);


				    if (strcmp($oldWurcs, $wurcs[1]) === 0) {
				    	echo ">> WURCSs are exact match".PHP_EOL;
				    }
				    else {
							echo ">> WURCS does not match.".PHP_EOL;
				    }



				    // Anomeric configuration
				    if(strpos($wurcs[1],'-1u') !== false){
	 			  	  echo ">> anomeric configuration (absolute representation, up)".PHP_EOL;
	 			  	  array_push($result, ">> anomeric configuration (absolute representation, up)");
	 			  	  $aglycon = 1;
	 			    }
	 			    if(strpos($wurcs[1],'-1d') !== false){
	 			  	  echo ">> anomeric configuration (absolute representation, down)".PHP_EOL;
	 			  	  array_push($result, ">> anomeric configuration (absolute representation, down)");
	 			  	  $aglycon = 1;
	 			    }

				    if(strpos($wurcs[1],'-2u') !== false){
	 			  	  echo ">> anomeric configuration (absolute representation, up)".PHP_EOL;
	 			  	  array_push($result, ">> anomeric configuration (absolute representation, up)");
	 			  	  $aglycon = 1;
	 			    }
	 			    if(strpos($wurcs[1],'-2d') !== false){
	 			  	  echo ">> anomeric configuration (absolute representation, down)".PHP_EOL;
	 			  	  array_push($result, ">> anomeric configuration (absolute representation, down)");
	 			  	  $aglycon = 1;
	 			    }


				  }
				}
				$opt = array();
				
	 			// https://gitlab.com/glycoinfo/molwurcs/-/blob/master/src/main/java/org/glycoinfo/MolWURCS/util/analysis/MoleculeNormalizer.java

	 			// isotope
	 			  if(strpos($sdf,'M  ISO') !== false){
	 			  	echo ">> molecule contains isotope data".PHP_EOL;
	 			  	array_push($result, ">> molecule contains isotope data");
	 			  	$aglycon = 1;
	 			  }
	 			
	 				// ionic
	 			  if(strpos($sdf,'M  CHG') !== false){
	 			  	echo ">> molecule contains charge data".PHP_EOL;
	 			  	array_push($result, ">> molecule contains charge data");
	 			  	$aglycon = 1;
	 			  }

  	 			// radical is not support
	 			  if(strpos($sdf,'M  RAD') !== false){
	 			  	echo ">> molecule contains radical data".PHP_EOL;
	 			  	array_push($result, ">> molecule contains radical data");
	 			  	$aglycon = 1;
	 			  }

	  			// metal
	 				// Li, Be, Na, Mg, K, Ca, Rb, Sr, Cs, Ba
	 			  if(strpos($sdf,'0.0000 Li') !== false){
	 			  	echo ">> molecule contains Li (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Li (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Be') !== false){
	 			  	echo ">> molecule contains Be (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Be (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Na') !== false){
	 			  	echo ">> molecule contains Na (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Na (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Mg') !== false){
	 			  	echo ">> molecule contains Mg (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Mg (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 K') !== false){
	 			  	echo ">> molecule contains K (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains K (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Ca') !== false){
	 			  	echo ">> molecule contains Ca (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Ca (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Rb') !== false){
	 			  	echo ">> molecule contains Rb (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Rb (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Sr') !== false){
	 			  	echo ">> molecule contains Sr (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Sr (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Cs') !== false){
	 			  	echo ">> molecule contains Cs (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Cs (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }
	 			  if(strpos($sdf,'0.0000 Ba') !== false){
	 			  	echo ">> molecule contains Ba (Metal atoms are replaced with hydrogen.)".PHP_EOL;
	 			  	array_push($result, ">> molecule contains Ba (Metal atoms are replaced with hydrogen.)");
	 			  	$aglycon = 1;
	 			  }


	 			  // aglycon ?
	 			  if ($aglycon === 0){
		 				echo ">> the structure of aglycon ?".PHP_EOL;
		 				array_push($result, ">> the structure of aglycon ?");
		 			}
		 			$aglycon = 0;

					
	 		}

  		$resultunique = array_unique($result);
			$result_values = array_values($resultunique);

			array_push($result_sum, $result_values);


	 		//$cidStrings = "CIDs: ";
	 		$unique = array_unique($ws);
	 		$ws_unique = array_values($unique);
	 		$log = ">> number of WURCS:".count($ws_unique)."\t number of CID:".count($cids)."\t";
	 		foreach ($cids as $cid) {
	 			$log .= $cid." ";
	 			//$cidStrings .= $cid." ";
	 		}
			$log .= PHP_EOL;
			echo $log;			

			//array_push($result_values, $cidStrings);
			//array_push($result_sum, $result_values);

    }
  }
}


echo "===================== summary =========================".PHP_EOL;
$line = 1;
foreach ($result_sum as $res) {
	echo "# ".$line." CID: ";
	$line++;
	foreach ($res as $value) {
		echo "\t".$value.PHP_EOL;
	}
}
echo "======================= end ===========================".PHP_EOL;

fclose($file);
?>